option(ENABLE_TESTING "Enable Test Builds" ON)

# This is a "meta" target that is used to collect all tests
add_custom_target(all_tests)

function(target_test name target)

  # Add the test to the CTest registry
  add_test(NAME ${name} COMMAND ${target})

  # Make the overall test meta-target depend on this test
  add_dependencies(all_tests ${target})

endfunction()
