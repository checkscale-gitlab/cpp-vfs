option(ENABLE_CPPCHECK "Enable static analysis with cppcheck during build process" OFF)

if(NOT CPPCHECK_REPORT_TYPE)
  set(CPPCHECK_REPORT_TYPE "None" CACHE STRING "Choose the type of CppCheck report." FORCE)
  set_property(CACHE CPPCHECK_REPORT_TYPE
    PROPERTY STRINGS
      "CodeQuality"
      "HTML"
      "JUnit"
      "None"
  )
  mark_as_advanced(CPPCHECK_REPORT_TYPE)
endif()

list(APPEND CPPCHECK_ARGS
  --enable=all
  --inconclusive
  --inline-suppr
  --language=c++
  --library=boost,bsd,gnu,posix,windows
  --quiet
  --std=c++20
  --suppressions-list=${CMAKE_SOURCE_DIR}/.cppcheck
  --verbose
)

macro(_enable_cppcheck_inbuild)
  set(CMAKE_CXX_CPPCHECK ${CPPCHECK} ${CPPCHECK_ARGS})
endmacro()

macro(_enable_cppcheck_report)
  set(CPPCHECKS_RESULTS ${CMAKE_BINARY_DIR}/cppcheck_results)
  file(MAKE_DIRECTORY ${CPPCHECKS_RESULTS})

  list(APPEND CPPCHECK_ARGS
    --output-file=${CPPCHECKS_RESULTS}/cppcheck_results.xml
    --project=${CMAKE_BINARY_DIR}/compile_commands.json
    --xml-version=2
      ${CMAKE_SOURCE_DIR}/project
  )

  add_custom_target(cppcheck
    COMMAND
      ${CPPCHECK} ${CPPCHECK_ARGS}
    COMMENT
      "Generate cppcheck report for the project"
  )

  if(${CPPCHECK_REPORT_TYPE} STREQUAL "CodeQuality")
    _enable_cppcheck_codequality_report()
  elseif(${CPPCHECK_REPORT_TYPE} STREQUAL "HTML")
    _enable_cppcheck_html_report()
  elseif(${CPPCHECK_REPORT_TYPE} STREQUAL "JUnit")
    _enable_cppcheck_junit_report()
  endif()
endmacro()

# https://gitlab.com/ahogen/cppcheck-codequality
macro(_enable_cppcheck_codequality_report)
  find_program(CPPCHECK_CODEQUALITY "cppcheck-codequality")
  if(CPPCHECK_CODEQUALITY)
    add_custom_target(cppcheck-codequality
      COMMAND
        ${CPPCHECK_CODEQUALITY}
        --input-file=${CPPCHECKS_RESULTS}/cppcheck_results.xml
        --output-file=${CPPCHECKS_RESULTS}/cppcheck_codequality.json
      COMMENT
        "Convert cppcheck report to CodeQuality output"
    )
    add_dependencies(cppcheck-codequality cppcheck)
  else()
    message(SEND_ERROR "cppcheck-codequality requested but executable not found")
  endif()
endmacro()

macro(_enable_cppcheck_html_report)
  find_program(CPPCHECK_HTML "cppcheck-htmlreport")
  if(CPPCHECK_HTML)
    add_custom_target(cppcheck-html
      COMMAND
        ${CPPCHECK_HTML}
        --title=${CMAKE_PROJECT_NAME}
        --file=${CPPCHECKS_RESULTS}/cppcheck_results.xml
        --report-dir=${CPPCHECKS_RESULTS}
        --source-dir=${CMAKE_SOURCE_DIR}
      COMMENT
        "Convert cppcheck report to HTML output"
    )
    add_dependencies(cppcheck-html cppcheck)
  else()
    message(SEND_ERROR "cppcheck-htmlreport requested but executable not found")
  endif()
endmacro()

# https://github.com/johnthagen/cppcheck-junit
macro(_enable_cppcheck_junit_report)
  find_program(CPPCHECK_JUNIT "cppcheck_junit")
  if(CPPCHECK_JUNIT)
    add_custom_target(cppcheck-junit
      COMMAND
        ${CPPCHECK_JUNIT}
        ${CPPCHECKS_RESULTS}/cppcheck_results.xml
        ${CPPCHECKS_RESULTS}/cppcheck_junit.xml
      COMMENT
        "Convert cppcheck report to JUnit output"
    )
    add_dependencies(cppcheck-junit cppcheck)
  else()
    message(SEND_ERROR "cppcheck_junit requested but executable not found")
  endif()
endmacro()

find_program(CPPCHECK
  NAMES
    cppcheck
  DOC
    "Path to cppcheck executable"
)

if(ENABLE_CPPCHECK)
  if(CPPCHECK)
    _enable_cppcheck_inbuild()
  else()
    message(SEND_ERROR "cppcheck requested but executable not found")
  endif()
endif()

if(NOT ${CPPCHECK_REPORT_TYPE} STREQUAL "None")
  if(CPPCHECK)
    _enable_cppcheck_report()
  else()
    message(SEND_ERROR "cppcheck requested but executable not found")
  endif()
endif()
