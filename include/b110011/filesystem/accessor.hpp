/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_ACCESSOR_HPP__
#define __B110011_FILESYSTEM_ACCESSOR_HPP__

#include "b110011/noncopyable.hpp"

#include <memory>

namespace b110011 {
namespace filesystem {

class FileFactory;
class FileSystemModel;

class Accessor
    :   private NonCopyable
{

public:

    [[nodiscard]]
    virtual FileFactory & takeFileFactory () = 0;


    [[nodiscard]]
    virtual FileSystemModel const & getFileSystem () const = 0;

    [[nodiscard]]
    virtual FileSystemModel & takeFileSystem () = 0;


    [[nodiscard]]
    static std::unique_ptr< Accessor > createUniqueAccessor ();

    [[nodiscard]]
    static std::shared_ptr< Accessor > createSharedAccessor ();

};

} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_ACCESSOR_HPP__
