/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_STREAM_STREAM_HPP__
#define __B110011_FILESYSTEM_STREAM_STREAM_HPP__

#include <filesystem>
#include <fstream>
#include <memory>

namespace b110011 {
namespace filesystem {

class StreamSentinel;

template< typename _StreamType >
class Stream
    :   public _StreamType
{

    friend class StreamFactoryProxy;

public:

    Stream ( Stream && _other )
        :   _StreamType{ std::move( _other ) }
        ,   m_pSentinel{ std::move( _other.m_pSentinel ) }
    {}

private:

    explicit Stream ()
    {}

    explicit Stream ( std::shared_ptr< StreamSentinel > _pSentinel )
        :   m_pSentinel{ _pSentinel }
    {}

    Stream ( std::filesystem::path const & _path, std::ios::openmode _mode )
        :   _StreamType{ _path, _mode }
    {}

private:

    std::shared_ptr< StreamSentinel > m_pSentinel;

};

using FileStream = Stream< std::fstream >;
using InputFileStream = Stream< std::ifstream >;
using OutputFileStream = Stream< std::ofstream >;

} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_STREAM_STREAM_HPP__
