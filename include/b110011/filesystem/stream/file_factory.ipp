/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_STREAM_FILEFACTORY_IPP__
#define __B110011_FILESYSTEM_STREAM_FILEFACTORY_IPP__

#ifndef __B110011_FILESYSTEM_STREAM_FILEFACTORY_HPP__
#include "b110011/filesystem/stream/file_factory.hpp"
#endif

namespace b110011 {
namespace filesystem {

FileStream
FileFactory::createFile (
    std::filesystem::path const & _path,
    std::ios::openmode _mode
)
{
    std::error_code ec;
    auto stream = createFile( _path, _mode, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ "cannot create r/w stream", _path, ec };
    }

    return stream;
}

InputFileStream
FileFactory::createInputFile (
    std::filesystem::path const & _path,
    std::ios::openmode _mode
)
{
    std::error_code ec;
    auto stream = createInputFile( _path, _mode, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ "cannot create read-only stream", _path, ec };
    }

    return stream;
}

OutputFileStream
FileFactory::createOutputFile (
    std::filesystem::path const & _path,
    std::ios::openmode _mode
)
{
    std::error_code ec;
    auto stream = createOutputFile( _path, _mode, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ "cannot create write-only stream", _path, ec };
    }

    return stream;
}

} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_STREAM_FILEFACTORY_IPP__
