/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_INVOKER_HPP__
#define __B110011_FILESYSTEM_INVOKER_HPP__

#include <filesystem>

namespace b110011 {
namespace filesystem {

template< typename _ChildT >
class Invoker
    :   public _ChildT
{

protected:

    template< typename _ResultT >
    using Callback = _ResultT ( _ChildT::* ) ( std::error_code & ) const;

    template< typename _ResultT >
    _ResultT invoke ( Callback< _ResultT > && _callback, char const * _error ) const
    {
        std::error_code ec;
        auto result = ( this->*_callback )( ec );

        if ( ec )
        {
            using error = std::filesystem::filesystem_error;
            throw error{ _error, getPath(), ec };
        }

        return result;
    }

    template< typename _ResultT >
    bool checkType ( Callback< _ResultT > && _callback ) const
    {
        return invoke( std::move( _callback ), "cannot get type" );
    }

};

} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_INVOKER_HPP__
