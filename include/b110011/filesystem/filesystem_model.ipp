/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_FILESYSTEMMODEL_IPP__
#define __B110011_FILESYSTEM_FILESYSTEMMODEL_IPP__

#ifndef __B110011_FILESYSTEM_FILESYSTEMMODEL_HPP__
#include "b110011/filesystem/filesystem_model.hpp"
#endif

namespace b110011 {
namespace filesystem {

template< typename _ResultT >
_ResultT
FileSystemModel::invoke (
    Callback< _ResultT > && _callback,
    std::filesystem::path const & _path,
    char const * _error
) const
{
    std::error_code ec;
    auto result = ( this->*_callback )( _path, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ _error, _path, ec };
    }

    return result;
}

inline std::uintmax_t
FileSystemModel::getFileSize ( std::filesystem::path const & _path ) const
{
    return invoke( &FileSystemModel::getFileSize, _path, "cannot get file size" );
}

inline std::filesystem::file_time_type
FileSystemModel::getLastWriteTime ( std::filesystem::path const & _path ) const
{
    return invoke( &FileSystemModel::getLastWriteTime, _path, "cannot get file time" );
}

inline void
FileSystemModel::setLastWriteTime (
    std::filesystem::path const & _path,
    std::filesystem::file_time_type _newTime
)
{
    std::error_code ec;
    setLastWriteTime( _path, _newTime, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ "cannot set file time", _path, ec };
    }
}

inline std::filesystem::space_info
FileSystemModel::getSpaceInfo ( std::filesystem::path const & _path ) const
{
    return invoke( &FileSystemModel::getSpaceInfo, _path, "cannot get free space" );
}

template< typename _ResultT >
bool
FileSystemModel::checkType (
    Callback< _ResultT > && _callback,
    std::filesystem::path const & _path
) const
{
    return invoke( std::move( _callback ), _path, "cannot get type" );
}

inline bool
FileSystemModel::isDirectory ( std::filesystem::path const & _path ) const
{
    std::error_code ec;
    auto result = isDirectory( _path, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ "cannot get type", _path, ec };
    }

    return result;
}

inline bool
FileSystemModel::isRegularFile ( std::filesystem::path const & _path ) const
{
    std::error_code ec;
    auto result = isRegularFile( _path, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ "cannot get type", _path, ec };
    }

    return result;
}

inline bool
FileSystemModel::exists ( std::filesystem::path const & _path ) const
{
    std::error_code ec;
    auto result = exists( _path, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ "cannot get status", _path, ec };
    }

    return result;
}

inline void
FileSystemModel::rename (
    std::filesystem::path const & _oldPath,
    std::filesystem::path const & _newPath
)
{
    std::error_code ec;
    rename( _oldPath, _newPath, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ "cannot remove", _oldPath, _newPath, ec };
    }
}

inline bool
FileSystemModel::remove ( std::filesystem::path const & _path )
{
    std::error_code ec;
    auto result = remove( _path, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ "cannot remove", _path, ec };
    }

    return result;
}

inline bool
FileSystemModel::createDirectory ( std::filesystem::path const & _path )
{
    std::error_code ec;
    auto result = createDirectory( _path, ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ "cannot create directory", _path, ec };
    }

    return result;
}

} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_FILESYSTEMMODEL_IPP__
