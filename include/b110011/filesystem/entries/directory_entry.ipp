/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_ENTRIES_DIRECTORYENTRY_IPP__
#define __B110011_FILESYSTEM_ENTRIES_DIRECTORYENTRY_IPP__

#ifndef __B110011_FILESYSTEM_ENTRIES_DIRECTORYENTRY_HPP__
#include "b110011/filesystem/entries/directory_entry.hpp"
#endif

namespace b110011 {
namespace filesystem {

template< typename _ResultT >
_ResultT
DirectoryEntry::invoke ( Callback< _ResultT > && _callback, char const * _error ) const
{
    std::error_code ec;
    auto result = ( this->*_callback )( ec );

    if ( ec )
    {
        using error = std::filesystem::filesystem_error;
        throw error{ _error, getPath(), ec };
    }

    return result;
}

inline std::filesystem::file_time_type
DirectoryEntry::getLastWriteTime () const
{
    return invoke( &DirectoryEntry::getLastWriteTime, "cannot get file time" );
}

template< typename _ResultT >
bool
DirectoryEntry::checkType ( Callback< _ResultT > && _callback ) const
{
    return invoke( std::move( _callback ), "cannot get type" );
}

inline bool
DirectoryEntry::isBlockFile () const
{
    return checkType( &DirectoryEntry::isBlockFile );
}

inline bool
DirectoryEntry::isCharacterFile () const
{
    return checkType( &DirectoryEntry::isCharacterFile );
}

inline bool
DirectoryEntry::isDirectory () const
{
    return checkType( &DirectoryEntry::isDirectory );
}

inline bool
DirectoryEntry::isFifo () const
{
    return checkType( &DirectoryEntry::isFifo );
}

inline bool
DirectoryEntry::isRegularFile () const
{
    return checkType( &DirectoryEntry::isRegularFile );
}

inline bool
DirectoryEntry::isSocket () const
{
    return checkType( &DirectoryEntry::isSocket );
}

inline bool
DirectoryEntry::isSymlink () const
{
    return checkType( &DirectoryEntry::isSymlink );
}

inline bool
DirectoryEntry::exists () const
{
    return invoke( &DirectoryEntry::exists, "cannot get status" );
}

inline bool
DirectoryEntry::operator == ( DirectoryEntry const & _rhs ) const noexcept
{
    return getPath() == _rhs.getPath();
}

#if __cpp_lib_three_way_comparison
inline std::strong_ordering
DirectoryEntry::operator <=> ( DirectoryEntry const & _rhs ) const noexcept
{
    return getPath() <=> _rhs.getPath();
}
#else
inline bool
DirectoryEntry::operator != ( DirectoryEntry const & _rhs ) const noexcept
{
    return getPath() != _rhs.getPath();
}

inline bool
DirectoryEntry::operator < ( DirectoryEntry const & _rhs ) const noexcept
{
    return getPath() < _rhs.getPath();
}

inline bool
DirectoryEntry::operator <= ( DirectoryEntry const & _rhs ) const noexcept
{
    return getPath() <= _rhs.getPath();
}

inline bool
DirectoryEntry::operator > ( DirectoryEntry const & _rhs ) const noexcept
{
    return getPath() > _rhs.getPath();
}

inline bool
DirectoryEntry::operator >= ( DirectoryEntry const & _rhs ) const noexcept
{
    return getPath() >= _rhs.getPath();
}
#endif

} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_ENTRIES_DIRECTORYENTRY_IPP__
