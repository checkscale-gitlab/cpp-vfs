/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/entries/directory_entry_impl.hpp"

namespace b110011 {
namespace filesystem {
namespace native {

DirectoryEntryImpl::DirectoryEntryImpl (
    std::filesystem::directory_entry const & _entry
) noexcept
    :   m_entry{ _entry }
{}

std::filesystem::path const &
DirectoryEntryImpl::getPath () const noexcept
{
    return m_entry.path();
}

std::filesystem::file_time_type
DirectoryEntryImpl::getLastWriteTime ( std::error_code & _ec ) const
{
    return m_entry.last_write_time( _ec );
}

bool
DirectoryEntryImpl::isBlockFile ( std::error_code & _ec ) const noexcept
{
    return m_entry.is_block_file( _ec );
}

bool
DirectoryEntryImpl::isCharacterFile ( std::error_code & _ec ) const noexcept
{
    return m_entry.is_character_file( _ec );
}

bool
DirectoryEntryImpl::isDirectory ( std::error_code & _ec ) const
{
    return m_entry.is_directory( _ec );
}

bool
DirectoryEntryImpl::isFifo ( std::error_code & _ec ) const noexcept
{
    return m_entry.is_fifo( _ec );
}

bool
DirectoryEntryImpl::isOther ( std::error_code & _ec ) const noexcept
{
    return m_entry.is_other( _ec );
}

bool
DirectoryEntryImpl::isRegularFile ( std::error_code & _ec ) const
{
    return m_entry.is_regular_file( _ec );
}

bool
DirectoryEntryImpl::isSocket ( std::error_code & _ec ) const noexcept
{
    return m_entry.is_socket( _ec );
}

bool
DirectoryEntryImpl::isSymlink ( std::error_code & _ec ) const noexcept
{
    return m_entry.is_symlink( _ec );
}

bool
DirectoryEntryImpl::exists ( std::error_code & _ec ) const
{
    return m_entry.exists( _ec );
}

DirectoryEntryImpl::operator std::filesystem::path const & () const noexcept
{
    return m_entry;
}

} // namespace native
} // namespace filesystem
} // namespace b110011
