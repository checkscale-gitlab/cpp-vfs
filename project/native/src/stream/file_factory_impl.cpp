/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/stream/file_factory_impl.hpp"

#include "src/stream/stream_factory_proxy.hpp"

namespace b110011 {
namespace filesystem {
namespace native {

FileStream
FileFactoryImpl::createFile (
    std::filesystem::path const & _path,
    std::ios::openmode _mode,
    std::error_code & _ec
)
{
    _ec.clear();
    return StreamFactoryProxy::createFile( _path, _mode );
}

InputFileStream
FileFactoryImpl::createInputFile (
    std::filesystem::path const & _path,
    std::ios::openmode _mode,
    std::error_code & _ec
)
{
    _ec.clear();
    return StreamFactoryProxy::createInputFile( _path, _mode );
}

OutputFileStream
FileFactoryImpl::createOutputFile (
    std::filesystem::path const & _path,
    std::ios::openmode _mode,
    std::error_code & _ec
)
{
    _ec.clear();
    return StreamFactoryProxy::createOutputFile( _path, _mode );
}

} // namespace native
} // namespace filesystem
} // namespace b110011
