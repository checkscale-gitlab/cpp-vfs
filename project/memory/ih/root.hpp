/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_IH_ROOT_HPP__
#define __B110011_FILESYSTEM_MEMORY_IH_ROOT_HPP__

#include "b110011/noncopyable.hpp"

#include <filesystem>

namespace b110011 {
namespace filesystem {
namespace memory {

class Entry;

class Root
    :   private NonCopyable
{

public:

    [[nodiscard]]
    virtual bool exists (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const = 0;


    [[nodiscard]]
    virtual Entry const * getEntry (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const = 0;

    [[nodiscard]]
    virtual Entry * takeEntry (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) = 0;

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_IH_ROOT_HPP__
