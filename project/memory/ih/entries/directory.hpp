/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_IH_ENTRIES_DIRECTORY_HPP__
#define __B110011_FILESYSTEM_MEMORY_IH_ENTRIES_DIRECTORY_HPP__

#include "ih/entries/entry.hpp"

#include <functional>
#include <memory>

namespace b110011 {
namespace filesystem {
namespace memory {

class Directory
    :   public Entry
{

public:

    [[nodiscard]]
    virtual bool hasEntry ( std::filesystem::path const & _name ) const = 0;


    [[nodiscard]]
    virtual Entry const * find ( std::filesystem::path const & _name ) const = 0;

    [[nodiscard]]
    virtual Entry * find ( std::filesystem::path const & _name ) = 0;


    using EntryCallback = std::function< bool ( std::shared_ptr< Entry const > ) >;

    virtual bool forEachEntry ( EntryCallback _callback ) const = 0;


    virtual bool add ( std::shared_ptr< Entry > _pEntry ) = 0;


    virtual bool rename (
        std::filesystem::path const & _oldName,
        std::filesystem::path const & _newName
    ) = 0;


    virtual bool remove ( std::filesystem::path const & _name ) = 0;

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_IH_ENTRIES_DIRECTORY_HPP__
