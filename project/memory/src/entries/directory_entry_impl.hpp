/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_DIRECTORYENTRYIMPL_HPP__
#define __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_DIRECTORYENTRYIMPL_HPP__

#include "b110011/filesystem/entries/directory_entry.hpp"

namespace b110011 {
namespace filesystem {
namespace memory {

class Entry;

class DirectoryEntryImpl final
    :   public DirectoryEntry
{

public:

    explicit DirectoryEntryImpl ( std::shared_ptr< Entry const > _pEntry ) noexcept;


    std::filesystem::path const & getPath () const override;

    std::filesystem::file_time_type getLastWriteTime (
        std::error_code & _ec
    ) const noexcept override;


    bool isBlockFile ( std::error_code & _ec ) const noexcept override;

    bool isCharacterFile ( std::error_code & _ec ) const noexcept override;

    bool isDirectory ( std::error_code & _ec ) const noexcept override;

    bool isFifo ( std::error_code & _ec ) const noexcept override;

    bool isOther ( std::error_code & _ec ) const noexcept override;

    bool isRegularFile ( std::error_code & _ec ) const noexcept override;

    bool isSocket ( std::error_code & _ec ) const noexcept override;

    bool isSymlink ( std::error_code & _ec ) const noexcept override;


    bool exists ( std::error_code & _ec ) const noexcept override;


    operator std::filesystem::path const & () const override;

private:

    static bool unsupported ( std::error_code & _ec ) noexcept;

private:

    std::weak_ptr< Entry const > m_pWeakEntry;

    std::filesystem::file_time_type m_cachedLastWriteTime;
    std::filesystem::file_type m_cachedType;

    mutable std::filesystem::path m_cachedPath;

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_DIRECTORYENTRYIMPL_HPP__
