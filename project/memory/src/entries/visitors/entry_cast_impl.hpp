/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_VISITORS_ENTRYCASTIMPL_HPP__
#define __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_VISITORS_ENTRYCASTIMPL_HPP__

#include "ih/entries/visitors/modifiable_visitor.hpp"
#include "ih/entries/visitors/read_only_visitor.hpp"
#include "ih/entries/entry.hpp"

#include <type_traits>

namespace b110011 {
namespace filesystem {
namespace memory {

template< typename _ResultT >
class EntryCastImpl final
    :   private ModifiableVisitor
    ,   private ReadOnlyVisitor
{

public:

    EntryCastImpl () noexcept
        :   m_pResult{ nullptr }
    {}


    _ResultT const * operator () ( Entry const & _entry )
    {
        m_pConstResult = nullptr;
        _entry.accept( *this );
        return m_pConstResult;
    }

    _ResultT * operator () ( Entry & _entry )
    {
        m_pResult = nullptr;
        _entry.accept( *this );
        return m_pResult;
    }

private:

    void visit ( Directory const & _directory ) noexcept override
    {
        if constexpr ( std::is_same_v< _ResultT, Directory > )
        {
            m_pConstResult = &_directory;
        }
    }

    void visit ( Directory & _directory ) noexcept override
    {
        if constexpr ( std::is_same_v< _ResultT, Directory > )
        {
            m_pResult = &_directory;
        }
    }


    void visit ( File const & _file ) noexcept override
    {
        if constexpr ( std::is_same_v< _ResultT, File > )
        {
            m_pConstResult = &_file;
        }
    }

    void visit ( File & _file ) noexcept override
    {
        if constexpr ( std::is_same_v< _ResultT, File > )
        {
            m_pResult = &_file;
        }
    }

private:

    union
    {
        _ResultT const * m_pConstResult;
        _ResultT * m_pResult;
    };

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_VISITORS_ENTRYCASTIMPL_HPP__
