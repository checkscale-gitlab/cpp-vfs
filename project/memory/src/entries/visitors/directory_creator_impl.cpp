/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/entries/visitors/directory_creator_impl.hpp"

#include "src/entries/directory_impl.hpp"

namespace b110011 {
namespace filesystem {
namespace memory {

DirectoryCreatorImpl::DirectoryCreatorImpl () noexcept
    :   m_pPath{ nullptr }
    ,   m_result{ false }
{}

std::error_code
DirectoryCreatorImpl::getError () const noexcept
{
    return m_error;
}

bool
DirectoryCreatorImpl::create ( Entry & _parentEntry, std::filesystem::path const & _path )
{
    m_pPath = &_path;

    _parentEntry.accept( *this );

    return m_result;
}

void
DirectoryCreatorImpl::visit ( Directory & _directory )
{
    auto name{ m_pPath->filename() };

    if ( _directory.hasEntry( name ) )
    {
        m_result = false;
        return;
    }

    _directory.add( std::make_shared< DirectoryImpl >( &_directory, std::move( name ) ) );
    m_result = true;
}

void
DirectoryCreatorImpl::visit ( File & )
{
    m_error = std::make_error_code( std::errc::not_a_directory );
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
