/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_VISITORS_ENTRYSEARCHERIMPL_HPP__
#define __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_VISITORS_ENTRYSEARCHERIMPL_HPP__

#include "ih/entries/visitors/modifiable_visitor.hpp"

#include <filesystem>

namespace b110011 {
namespace filesystem {
namespace memory {

class Entry;

class EntrySearcherImpl final
    :   private ModifiableVisitor
{

public:

    EntrySearcherImpl ( Entry & _startingEntry ) noexcept;

    std::error_code getError () const noexcept;

    Entry * find ( std::filesystem::path const & _path );

private:

    void visit ( Directory & _directory ) override;

    void visit ( File & _file ) override;

private:

    Entry * m_pCurrentEntry;

    std::filesystem::path::const_iterator m_itCurrentElement;

    std::error_code m_error;
};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_VISITORS_ENTRYSEARCHERIMPL_HPP__
