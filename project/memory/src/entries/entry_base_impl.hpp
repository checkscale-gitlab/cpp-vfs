/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_ENTRYBASEIMPL_HPP__
#define __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_ENTRYBASEIMPL_HPP__

namespace b110011 {
namespace filesystem {
namespace memory {

class Directory;
class ModifiableVisitor;
class ReadOnlyVisitor;

template< class _Entry >
class EntryBaseImpl
    :   public _Entry
{

public:

    EntryBaseImpl (
        Directory * _pParent,
        std::filesystem::path _name,
        std::filesystem::file_type _type
    );


    std::uintmax_t getSize () const override;


    std::filesystem::path const & getName () const noexcept override;

    void setName ( std::filesystem::path const & _newName ) override;


    std::filesystem::path getFullPath () const override;


    Directory const * getParentDirectory () const noexcept override;

    Directory * takeParentDirectory () noexcept override;

    void setParentDirectory ( Directory * _pNewParent ) noexcept override;


    std::filesystem::file_time_type getLastModificationTime () const noexcept override;

    void setLastModificationTime (
        std::filesystem::file_time_type _newTime
    ) noexcept override;


    std::filesystem::file_type getType () const noexcept override;


    void accept ( ModifiableVisitor & _visitor ) override;

    void accept ( ReadOnlyVisitor & _visitor ) const override;

private:

    Directory * m_parent;

    std::filesystem::file_time_type m_lastModificationTime;
    std::filesystem::file_type m_type;
    std::filesystem::path m_name;

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_ENTRYBASEIMPL_HPP__
