/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/stream/file_descriptors_manager.hpp"

#include "src/stream/file_descriptor.hpp"
#include "src/stream/stream_factory_proxy.hpp"

#include <cassert>
#include <fstream>

namespace b110011 {
namespace filesystem {
namespace memory {

FileDescriptorsManager::FileDescriptorsManager ( std::streambuf & _mainFileBuffer )
    :   m_mainFileBuffer{ _mainFileBuffer }
    ,   m_inHandlersCount{ 0 }
    ,   m_hasOutDescriptor{ false }
    ,   m_hasInoutDescriptor{ false }
{}

FileDescriptorsManager::~FileDescriptorsManager ()
{
    removeAllHandlers();
}

FileStream
FileDescriptorsManager::createStream ( std::ios::openmode _mode, std::error_code _ec )
{
    if ( isModifiableMode( _mode ) && hasHandlers() )
    {
        _ec = std::make_error_code( std::errc::no_such_file_or_directory );
        return StreamFactoryProxy::createEmptyStream< std::fstream >();
    }
    _ec.clear();

    if ( _mode & ( std::ios::in | std::ios::out ) )
    {
        m_hasInoutDescriptor = true;
    }
    else if ( _mode & std::ios::in )
    {
        m_inHandlersCount += 1;
    }
    else if ( _mode & std::ios::out )
    {
        m_hasOutDescriptor = true;
    }

    return createStream< std::fstream >( _mode );
}

InputFileStream
FileDescriptorsManager::createInputStream ( std::ios::openmode _mode, std::error_code _ec )
{
    if ( m_hasInoutDescriptor || m_hasOutDescriptor )
    {
        /* NOTE: Maybe not the best error code.
            Read https://unix.stackexchange.com/questions/577152/.
        */
        _ec = std::make_error_code( std::errc::text_file_busy );
        return StreamFactoryProxy::createEmptyStream< std::ifstream >();
    }
    _ec.clear();

    _mode &= ~std::ios::out;
    m_inHandlersCount += 1;

    return createStream< std::ifstream >( _mode );
}

OutputFileStream
FileDescriptorsManager::createOutputStream ( std::ios::openmode _mode, std::error_code _ec )
{
    if ( isModifiableMode( _mode ) && hasHandlers() )
    {
        /* NOTE: Maybe not the best error code.
            Read https://unix.stackexchange.com/questions/577152/.
        */
        _ec = std::make_error_code( std::errc::text_file_busy );
        return StreamFactoryProxy::createEmptyStream< std::ofstream >();
    }
    _ec.clear();

    _mode &= ~std::ios::in;
    _mode &= ~std::ios::trunc;

    m_hasOutDescriptor = true;

    return createStream< std::ofstream >( _mode );
}

bool
FileDescriptorsManager::hasHandlers () const noexcept
{
    return m_hasInoutDescriptor || m_inHandlersCount || m_hasOutDescriptor;
}

template< typename _StreamType >
Stream< _StreamType >
FileDescriptorsManager::createStream ( std::ios::openmode _mode )
{
    auto pSentinel{ std::make_shared< FileDescriptor >( *this, m_mainFileBuffer, _mode ) };
    m_handlers.insert( pSentinel );

    auto stream{ StreamFactoryProxy::createStream< _StreamType >( pSentinel ) };
    pSentinel->attach( stream );

    return stream;
}

void
FileDescriptorsManager::removeHandler (
    std::shared_ptr< FileDescriptor > _pHanlder,
    std::ios::openmode _mode
)
{
    if ( !m_handlers.erase( _pHanlder ) )
    {
        throw Exception{ "[VFS]: File descriptor, that should be removed, does not exist!", std::errc::bad_file_descriptor };
    }

    if ( _mode & ( std::ios::in | std::ios::out ) )
    {
        m_hasInoutDescriptor = false;
    }
    else if ( _mode & std::ios::in )
    {
        assert( m_inHandlersCount >= 1 );
        m_inHandlersCount -= 1;
    }
    else if ( _mode & std::ios::out )
    {
        m_hasOutDescriptor = false;
    }
}

void
FileDescriptorsManager::removeAllHandlers ()
{
    for ( auto && handler : m_handlers )
    {
        handler->detach();
    }

    m_handlers.clear();

    m_inHandlersCount = 0;
    m_hasInoutDescriptor = false;
    m_hasOutDescriptor = false;
}

bool
FileDescriptorsManager::isModifiableMode ( std::ios::openmode _mode ) noexcept
{
    switch ( _mode )
    {
        case std::ios::out:
        case std::ios::trunc:
            return true;

        default:
            return false;
    }
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
