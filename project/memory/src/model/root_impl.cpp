/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/model/root_impl.hpp"

#include "ih/entries/file.hpp"

#include "src/entries/visitors/entry_cast_impl.hpp"
#include "src/entries/visitors/entry_searcher_impl.hpp"
#include "src/entries/visitors/file_creator_impl.hpp"
#include "src/entries/directory_impl.hpp"
#include "src/stream/stream_factory_proxy.hpp"

#include <cassert>

namespace b110011 {
namespace filesystem {
namespace memory {

#ifdef _WIN32
static constexpr auto s_rootDirName = "M:";
#else
static constexpr auto s_rootDirName = "";
#endif

RootImpl::RootImpl ()
    :   m_pRoot{ std::make_unique< DirectoryImpl >( nullptr, s_rootDirName ) }
{}

RootImpl::~RootImpl () = default;

bool
RootImpl::exists (
    std::filesystem::path const & _path,
    std::error_code & _ec
) const
{
    assert( m_pRoot );
    return search( *m_pRoot, _path, _ec );
}

Entry const *
RootImpl::getEntry (
    std::filesystem::path const & _path,
    std::error_code & _ec
) const
{
    assert( m_pRoot );
    return search( *m_pRoot, _path, _ec );
}

Entry *
RootImpl::takeEntry (
    std::filesystem::path const & _path,
    std::error_code & _ec
)
{
    assert( m_pRoot );
    return search( *m_pRoot, _path, _ec );
}

FileStream
RootImpl::createFile (
    std::filesystem::path const & _path,
    std::ios::openmode _mode,
    std::error_code & _ec
)
{
    return createWriteStream( &File::createStream, _path, _mode, _ec );
}

InputFileStream
RootImpl::createInputFile (
    std::filesystem::path const & _path,
    std::ios::openmode _mode,
    std::error_code & _ec
)
{
    auto * pEntry = takeEntry( _path, _ec );

    if ( !_ec )
    {
        assert( pEntry );
        switch ( pEntry->getType() )
        {
            case std::filesystem::file_type::regular:
            {
                auto * pFile = EntryCastImpl< File >{}( *pEntry );
                assert( pFile );
                return pFile->createInputStream( _mode, _ec );
            }

            case std::filesystem::file_type::directory:
                _ec = std::make_error_code( std::errc::is_a_directory );
                break;

            default:
                _ec = std::make_error_code( std::errc::not_supported );
                break;
        }
    }

    return StreamFactoryProxy::createEmptyStream< std::ifstream >();
}

OutputFileStream
RootImpl::createOutputFile (
    std::filesystem::path const & _path,
    std::ios::openmode _mode,
    std::error_code & _ec
)
{
    return createWriteStream( &File::createOutputStream, _path, _mode, _ec );
}

Entry *
RootImpl::search (
    Entry & _root,
    std::filesystem::path const & _path,
    std::error_code & _ec
)
{
    EntrySearcherImpl search{ _root };
    Entry * pResult = search.find( _path );

    if ( auto error = search.getError() )
    {
        _ec = error;
    }
    else
    {
        _ec.clear();
    }

    return pResult;
}

template< typename _StreamT, typename... _Args >
Stream< _StreamT >
RootImpl::createWriteStream (
    StreamCreator< _StreamT, _Args... > && _creator,
    std::filesystem::path const & _path,
    std::ios::openmode _mode,
    std::error_code & _ec
)
{
    auto * pParent = takeEntry( _path.parent_path(), _ec );

    if ( !_ec )
    {
        assert( pParent );
        FileCreatorImpl creator{ *pParent };

        auto * pFile = creator.create( _path );
        auto error = creator.getError();
        if ( !pFile && error )
        {
            _ec = error;
        }
        else
        {
            assert( pFile );
            return ( pFile->*_creator )( _mode, _ec );
        }
    }

    return StreamFactoryProxy::createEmptyStream< _StreamT >();
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
