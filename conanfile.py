from conans import ConanFile, CMake

class CppVfsConan(ConanFile):
    version = "0.1.0"
    name = "cpp-vfs"
    description = "Simple virtual filesystem."
    license = "Apache License 2.0"
    url = "https://gitlab.com/b110011/cpp-vfs"
    exports_sources = "include/b110011/*", "CMakeLists.txt", "cmake/*", "LICENSE.txt"
    settings = "compiler", "build_type", "arch"
    build_policy = "missing"
    author = "Serhii Zinchenko"

    def build(self):
        """Avoid warning on build step"""
        pass

    def package(self):
        """Run CMake install"""
        cmake = CMake(self)
        cmake.configure()
        cmake.install()
